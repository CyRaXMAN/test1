from django.shortcuts import reverse, get_object_or_404
from django.utils.http import is_safe_url
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout
)
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from blog.models import Post, Category
from blog.forms import PostCommentForm, OpinionForm, LoginForm
from django.views.generic import DetailView, ListView, RedirectView
from django.views.generic.edit import FormMixin, FormView
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.db.models import Sum, Case, When, IntegerField


class PostListView(ListView):

    model = Post
    template_name = "blog/post_list.html"
    context_object_name = "entries"
    paginate_by = 3

    def get_queryset(self):
        queryset = super().get_queryset()
        category_id = self.kwargs.get('category_id')
        if category_id:
            category = get_object_or_404(Category, pk=category_id)
            return queryset.filter(categories=category)
        return queryset


class LoginView(FormView):

    template_name = "blog/login.html"
    form_class = LoginForm
    redirect_field_name = REDIRECT_FIELD_NAME

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        request.session.set_test_cookie()
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        auth_login(self.request, form.get_user())
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
        return super().form_valid(form)

    def get_success_url(self):
        redirect_to = self.request.POST.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = reverse('home_page')
        return redirect_to


class LogoutView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse('home_page')

    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return super().get(request, *args, **kwargs)


class PostView(FormMixin, DetailView):

    model = Post
    template_name = 'blog/post.html'
    context_object_name = 'entry'
    pk_url_kwarg = 'post_id'
    form_class = PostCommentForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post_object = kwargs.get("object")
        if self.request.user.is_authenticated:
            context['comment_exists'] = self.request.user.user_comments.filter(
                post=post_object
            ).exists()
        return context

    def get_success_url(self):
        post_object = self.get_object()
        return reverse('post_page', kwargs={'post_id': post_object.pk})

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def form_valid(self, form):
        post_object = self.get_object()
        comment = form.save(commit=False)
        comment.author = self.request.user
        comment.post = post_object
        comment.save()
        return super().form_valid(form)


@require_POST
def submit_opinion(request):
    if not request.user.is_authenticated():
        return JsonResponse({'ok': False, 'errors': ["You must login first"]})
    form = OpinionForm(request.POST)
    if form.is_valid():
        post = form.cleaned_data['post']
        if post.post_opinions.filter(author=request.user).exists():
            return JsonResponse(
                {'ok': False, 'errors': ["You have already voted"]}
            )
        opinion = form.save(commit=False)
        opinion.author = request.user
        opinion.save()
        return JsonResponse({'ok': True})
    return JsonResponse({'ok': False, 'errors': form.errors})