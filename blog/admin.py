from django.contrib import admin
from blog.models import Post, Category, PostComment, City, SiteSettings, Opinion

admin.site.register([Post, Category, PostComment, City, SiteSettings, Opinion])
