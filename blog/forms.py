from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django import forms

from blog.models import PostComment, Opinion


class PostCommentForm(ModelForm):

    class Meta:
        model = PostComment
        fields = ('content',)


class OpinionForm(ModelForm):

    class Meta:
        model = Opinion
        fields = ('opinion_type', 'post')


class LoginForm(AuthenticationForm):

    username = UsernameField(
        label="Login",
        max_length=254,
        widget=forms.TextInput(),
    )
    password = forms.CharField(
        label="Password",
        strip=False,
        widget=forms.PasswordInput(),
    )
