from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.db.models import Sum, Case, When, IntegerField


OPINION_TYPES = (('l', 'Like'), ('d', 'Dislike'))


class Category(models.Model):

    name = models.CharField(max_length=200)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class City(models.Model):

    name = models.CharField(max_length=200)

    class Meta:
        verbose_name = "City"
        verbose_name_plural = "Cities"

    def __str__(self):
        return self.name


class Post(models.Model):

    title = models.CharField(max_length=200)
    content = models.TextField(max_length=50000)
    author = models.ForeignKey(User)
    categories = models.ManyToManyField(Category, db_index=True)
    date_posted = models.DateTimeField(auto_now_add=True, db_index=True)
    city = models.ForeignKey(City, blank=True, null=True)
    image = models.ImageField(upload_to='post_images')

    @property
    def post_opinion_data(self):
        opinions = self.post_opinions.aggregate(
            likes=Sum(
                Case(When(opinion_type='l', then=1), default=0),
                output_field=IntegerField()
            ),
            dislikes=Sum(
                Case(When(opinion_type='d', then=1), default=0),
                output_field=IntegerField()
            )
        )
        return opinions

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"
        ordering = ('-date_posted',)

    def __str__(self):
        return 'Post "{}" from {}'.format(self.title, self.date_posted)


class PostComment(models.Model):

    author = models.ForeignKey(User, related_name='user_comments')
    content = models.TextField(max_length=1000)
    post = models.ForeignKey(Post, related_name='post_comments')
    date_posted = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Post comment"
        verbose_name_plural = "Post comments"
        unique_together = ('author', 'post')

    def __str__(self):
        return "{}: {:.10}...".format(self.author.username, self.content)


class Opinion(models.Model):

    author = models.ForeignKey(User, related_name='user_opinions')
    post = models.ForeignKey(Post, related_name='post_opinions')
    made = models.DateTimeField(auto_now_add=True)
    opinion_type = models.CharField(max_length=1, choices=OPINION_TYPES)

    class Meta:
        verbose_name = "Opinion"
        verbose_name_plural = "Opinions"
        unique_together = ('author', 'post')

    def __str__(self):
        return "{} {} {}".format(
            self.author.username, self.opinion_type, self.post.title
        )


class SiteSettings(models.Model):

    site = models.OneToOneField(Site)
    phone = models.CharField(max_length=20)
    email = models.EmailField(max_length=200)

    def __str__(self):
        return 'Settings for "{}"'.format(self.site.name)