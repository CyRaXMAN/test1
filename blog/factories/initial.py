from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from blog.models import Category, Post, City, SiteSettings
from factory import DjangoModelFactory, PostGenerationMethodCall


class UserFactory(DjangoModelFactory):

    password = PostGenerationMethodCall('set_password', '1')

    class Meta:
        model = User
        django_get_or_create = ('username',)


class CategoryFactory(DjangoModelFactory):

    class Meta:
        model = Category
        django_get_or_create = ('name',)


class PostFactory(DjangoModelFactory):

    class Meta:
        model = Post
        django_get_or_create = ('title',)


class CityFactory(DjangoModelFactory):

    class Meta:
        model = City
        django_get_or_create = ('name',)


class SiteFactory(DjangoModelFactory):

    class Meta:
        model = Site
        django_get_or_create = ('domain',)


class SiteSettingsFactory(DjangoModelFactory):

    class Meta:
        model = SiteSettings
        django_get_or_create = ('site',)
