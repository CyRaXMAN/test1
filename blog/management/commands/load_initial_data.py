from django.core.management.base import BaseCommand
from blog.factories.initial import (
    UserFactory, CategoryFactory, SiteFactory, SiteSettingsFactory,
    CityFactory
)


class Command(BaseCommand):
    help = 'Load initial data for app'

    def handle(self, *args, **options):
        UserFactory(
            email='admin@admin.com', username='admin', is_superuser=True,
            is_staff=True, is_active=True
        )
        UserFactory(
            email='test@test.com', username='testuser', is_superuser=False,
            is_staff=False, is_active=True
        )
        site = SiteFactory(domain="localhost", name="localhost")
        SiteSettingsFactory(
            site=site, email="admin@localhost", phone="+79001002030"
        )
        CategoryFactory(name="Test")
        CityFactory(name="Novosibirsk")