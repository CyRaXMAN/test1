import unittest.mock
from django.test import TestCase
from django.contrib.auth.models import User
from django.core.files import File
from blog.models import Post, Category


class PostTest(TestCase):

    def setUp(self):
        Category.objects.create(name="Test")
        User.objects.create(
            email='admin@admin.com', username='admin', is_superuser=True,
            is_staff=True, is_active=True
        )

    def test_create_post(self):
        category = Category.objects.get(name="Test")
        user = User.objects.get(username="admin")
        image = unittest.mock.MagicMock(spec=File)
        image.name = 'FileMock'
        post = Post(
            title="Test post", content="Test post created", author=user,
            image=image
        )
        post.save()
        self.assertEqual(post.title, "Test post")
        post.categories.add(category)
        post.save()
        self.assertIn(category, post.categories.all())
