from blog.models import Category, SiteSettings


def sidebar_data(request):
    data = dict()
    data['categories'] = Category.objects.all()
    data['site_settings'] = SiteSettings.objects.filter().first()
    return data