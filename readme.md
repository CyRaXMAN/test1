
Запуск в Docker:
docker build -t testapp .
docker run -p 8090:8090 -i -t -d testapp

Добавить тестовые данные:
python manage.py load_initial_data

Данные для входа:
admin/1
testuser/1

Основной сайт: localhost