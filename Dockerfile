FROM fedora:latest
LABEL maintainer <cyraxman0s@gmail.com>

RUN dnf install -y python3
RUN dnf groupinstall -y "Development tools"
RUN dnf install -y python3-pip libjpeg-devel
ADD . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
RUN python3 manage.py migrate
RUN python3 manage.py test
RUN python3 manage.py load_initial_data
EXPOSE 8090
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8090"]

